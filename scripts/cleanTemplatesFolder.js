const { execute } = require("../utils/utils");
const path = require('path')

const cleanTemplatesFolder = async () => {  
  try {
    await execute(`rm -rf ${path.join(__dirname, '..' ,'templates')}`);
  } catch (e) {
    throw new Error(e.message);
  }
}

module.exports = cleanTemplatesFolder
